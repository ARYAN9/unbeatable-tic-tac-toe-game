//Define UI Elements variables
const main_box = document.querySelector(".main-box");
const x_button = document.querySelector(".x-button");
const o_button = document.querySelector(".o-button");
const block_1 = document.querySelector(".block-1");
const block_2 = document.querySelector(".block-2");
const block_3 = document.querySelector(".block-3");
const block_4 = document.querySelector(".block-4");
const block_5 = document.querySelector(".block-5");
const block_6 = document.querySelector(".block-6");
const block_7 = document.querySelector(".block-7");
const block_8 = document.querySelector(".block-8");
const block_9 = document.querySelector(".block-9");
const center = document.querySelector(".center");
const restart = document.querySelector(".restart");
const reset = document.querySelector(".reset");
const cross_button = document.querySelector(".cross-button");
var user, computer, oddeven = 0;
var array_of_over_user = [];
var array_of_over_cpu = [];
var arrays = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]];
//This are the 8 possible combinations!!!

const gameover = document.querySelector(".gameover");
const single_player = document.querySelector(".single-player");
const two_player = document.querySelector(".two-player");
const options = document.querySelector(".options");
const player_selection = document.querySelector(".player-selection");
var one_or_two_players = false;
//If the boolean is true then the 2 players are playing else if false bydefault one player is playing 
const total_win = document.querySelector(".total-win");
const total_lose = document.querySelector(".total-lose");
const total_tied = document.querySelector(".total-tied");
const results = document.querySelector(".results");
const backgrd_res = document.querySelector(".backgrd_res");
const result_big = document.querySelector(".result-big");
const main_heading = document.querySelector(".main-heading");

main_box.style.display = 'none';
loadEventListeners();

//All the event listeners are loaded in this function
function loadEventListeners() {
    main_box.addEventListener("click", boxClicked);
    x_button.addEventListener("click", x_button_click);
    o_button.addEventListener("click", o_button_click);
    restart.addEventListener("click", restart_click);
    reset.addEventListener("click", reset_click);
    single_player.addEventListener("click", single_player_click);
    two_player.addEventListener("click", two_player_click);
    results.addEventListener("click", results_display);
    cross_button.addEventListener("click", cross_button_click);
}


function myMove(line_number, final_width, final_height) {
    var elem = document.querySelector(line_number);
    var pos = 0;
    if (final_height == 0) {
        var id = setInterval(frame, 5);

        function frame() {
            if (pos == final_width) {
                clearInterval(id);
            } else {
                pos++;
                elem.style.width = pos + "px";
            }
        }
    } else {
        var id = setInterval(frame, 5);

        function frame() {
            if (pos == final_height) {
                clearInterval(id);
            } else {
                pos++;
                elem.style.height = pos + "px";
            }
        }
    }
}

function displayLine() {
    if ((block_1.textContent == user && block_2.textContent == user && block_3.textContent == user) || (block_1.textContent == computer && block_2.textContent == computer && block_3.textContent == computer)) {
        myMove(".line1", 236, 0);
    } else if ((block_4.textContent == user && block_5.textContent == user && block_6.textContent == user) || (block_4.textContent == computer && block_5.textContent == computer && block_6.textContent == computer)) {
        myMove(".line2", 236, 0);
    } else if ((block_7.textContent == user && block_8.textContent == user && block_9.textContent == user) || (block_7.textContent == computer && block_8.textContent == computer && block_9.textContent == computer)) {
        myMove(".line3", 236, 0);
    } else if ((block_1.textContent == user && block_4.textContent == user && block_7.textContent == user) || (block_1.textContent == computer && block_4.textContent == computer && block_7.textContent == computer)) {
        myMove(".line4", 236, 220);
    } else if ((block_2.textContent == user && block_5.textContent == user && block_8.textContent == user) || (block_2.textContent == computer && block_5.textContent == computer && block_8.textContent == computer)) {
        myMove(".line5", 236, 220);
    } else if ((block_3.textContent == user && block_6.textContent == user && block_9.textContent == user) || (block_3.textContent == computer && block_6.textContent == computer && block_9.textContent == computer)) {
        myMove(".line6", 236, 220);
    } else if ((block_3.textContent == user && block_5.textContent == user && block_7.textContent == user) || (block_3.textContent == computer && block_5.textContent == computer && block_7.textContent == computer)) {
        myMove(".line7", 236, 280);
    } else if ((block_1.textContent == user && block_5.textContent == user && block_9.textContent == user) || (block_1.textContent == computer && block_5.textContent == computer && block_9.textContent == computer)) {
        myMove(".line8", 236, 280);
    }
}

function single_player_click() {
    options.style.display = 'block';
    player_selection.style.display = 'none';
}

function two_player_click(e) {
    main_box.style.display = 'grid';
    player_selection.style.display = 'none';
    options.style.display = 'block';
    x_button.style.display = 'none';
    o_button.style.display = 'none';
    center.style.display = 'none';
    restart.style.display = 'inline-block';
    reset.style.display = 'inline-block';
    one_or_two_players = true;
}

function boxClicked(e) {
    if (one_or_two_players == false) {
        //means that the single player is playing!!!
        if (((document.querySelector(".block-" + (e.target.classList[1].charAt(6)))).textContent == "") && (gameover.textContent == "")) {
            //That block is not already fill and game is not over then go inside this!!!
            for (var j = 1; j < 10; j++) {
                var name = "block-" + (j);
                if (e.target.classList[1] == (name)) {
                    array_of_over_user.push(j);
                    if (array_of_over_user.length >= 3) {
                        for (var i = 0; i < arrays.length; i++) {
                            if (containsOtherArray(array_of_over_user, arrays[i])) {
                                //win
                                e.target.textContent = user;
                                gameover.textContent = "You win the game!!!";
                                main_heading.style.display = 'none';
                                displayLine();
                                gameover.style.display = 'block';
                                main_heading.style.display = 'none';
                                const win = localStorage.getItem("win");
                                //It will return 0 ,even if the win is not declared with the setItem!!!
                                const totalwins = Number(win) + 1;
                                localStorage.setItem('win', totalwins);
                                return;
                            }
                        }
                    }
                }
            }
            e.target.textContent = user;
            if (!(((array_of_over_user.length) + (array_of_over_cpu.length)) == 9)) {
                computer_generate();
            } else {
                gameover.textContent = "Game Tied!!!";
                gameover.style.display = 'block';
                main_heading.style.display = 'none';
                const tied = localStorage.getItem("tied");
                //It will return 0 ,even if the tied is not declared with the setItem!!!
                const totalties = Number(tied) + 1;
                localStorage.setItem('tied', totalties);
            }

        }
    } else {
        //It means that there is twoPlayers playing!!!
        user = "X";
        computer = "0";
        if (((document.querySelector("." + (e.target.classList[1]))).textContent == "") && (gameover.textContent == "")) {
            oddeven++;
            if (oddeven & 1) {
                //odd
                for (var j = 1; j < 10; j++) {
                    var name = "block-" + (j);
                    if (e.target.classList[1] == (name)) {
                        array_of_over_user.push(j);
                        if (array_of_over_user.length >= 3) {
                            for (var i = 0; i < arrays.length; i++) {
                                if (containsOtherArray(array_of_over_user, arrays[i])) {
                                    //win
                                    e.target.textContent = user;
                                    gameover.textContent = "Player1 win the game!!!";
                                    displayLine();
                                    gameover.style.display = 'block';
                                    main_heading.style.display = 'none';

                                    return;
                                }
                            }
                        }
                    }
                }
                e.target.textContent = user;
                if ((((array_of_over_user.length) + (array_of_over_cpu.length)) == 9)) {
                    gameover.textContent = "Game Tied!!!";
                    gameover.style.display = 'block';
                    main_heading.style.display = 'none';

                }
            } else {
                //even
                for (var j = 1; j < 10; j++) {
                    var name = "block-" + (j);
                    if (e.target.classList[1] == (name)) {
                        array_of_over_cpu.push(j);
                        if (array_of_over_cpu.length >= 3) {
                            for (var i = 0; i < arrays.length; i++) {
                                if (containsOtherArray(array_of_over_cpu, arrays[i])) {
                                    //win
                                    e.target.textContent = computer;
                                    gameover.textContent = "Player2 win the game!!!";
                                    displayLine();
                                    gameover.style.display = 'block';
                                    main_heading.style.display = 'none';
                                    return;
                                }
                            }
                        }
                    }
                }
                e.target.textContent = computer;
            }
        }
    }
}

function two_cross() {
    for (var i = 0; i < arrays.length; i++) {
        var completed = [];
        var index = 0;
        var count = 0;
        for (var k = 0; k < arrays[i].length; k++) {
            for (var j = 0; j < array_of_over_user.length; j++) {
                if ((arrays[i][k] == (array_of_over_user[j])) && (!(array_of_over_cpu.includes(arrays[i][k])))) {
                    count++;
                    completed[index++] = array_of_over_user[j];
                    if (count == 2) {
                        for (var outer = 0; outer < arrays[i].length; outer++) {
                            var flag = false;
                            for (var inner = 0; inner < index; inner++) {
                                if (arrays[i][outer] == completed[inner])
                                    flag = true;
                            }
                            if (flag == false) {
                                if ((!(array_of_over_cpu.includes(arrays[i][outer]))))
                                    return arrays[i][outer];
                            }

                        }
                    }

                }
            }
        }
    }
    return -1;
}

function two_cross_computer() {

    for (var i = 0; i < arrays.length; i++) {
        var completed = [];
        var index = 0;
        var count = 0;
        for (var k = 0; k < arrays[i].length; k++) {
            for (var j = 0; j < array_of_over_cpu.length; j++) {
                if ((arrays[i][k] == (array_of_over_cpu[j])) && (!(array_of_over_user.includes(arrays[i][k])))) {
                    count++;
                    completed[index++] = array_of_over_cpu[j];
                    if (count == 2) {
                        for (var outer = 0; outer < arrays[i].length; outer++) {
                            var flag = false;
                            for (var inner = 0; inner < index; inner++) {
                                if (arrays[i][outer] == completed[inner])
                                    flag = true;
                            }
                            if (flag == false) {
                                if ((!(array_of_over_user.includes(arrays[i][outer]))))
                                    return arrays[i][outer];
                            }

                        }
                    }
                }
            }
        }
    }
    return -1;
}

function two_border_over() {
    var borders = [1, 3, 7, 9];
    var count = 0;
    for (var i = 0; i < borders.length; i++) {
        if ((array_of_over_cpu.includes(borders[i])) || array_of_over_user.includes(borders[i])) {
            count++;
        }
    }
    if (count >= 2) {
        return true;
    }
    return false;
}

function random_number_excluding_border() {
    var temp_array = [2, 4, 6, 8];
    do {
        var index = (1 + Math.round(Math.random() * 3));
        index -= 1;
    } while ((array_of_over_user.includes(temp_array[index])) || (array_of_over_cpu.includes(temp_array[index])));
    return temp_array[index];
}

function any_border_remaining() {
    var borders = [1, 3, 7, 9];
    for (var i = 0; i < borders.length; i++) {
        if (!((array_of_over_user.includes(borders[i])) || (array_of_over_cpu.includes(borders[i])))) {
            return borders[i];
        }
    }
    return -1;
}

function computer_generate() {
    var two_cross_array, remaining_border, two_computer;
    do {
        if (((block_5).textContent) == "") {
            var random_number = 5;
        } else if ((two_computer = two_cross_computer()) != -1) {
            var random_number = two_computer;
        } else if ((two_cross_array = two_cross()) != -1) {
            var random_number = two_cross_array;
        } else if ((block_7 == user) && (block_6 == user) && (block_4 == "")) {
            var random_number = 9;
        } else if (((block_1.textContent) == user) && (block_8.textContent == user) && (block_4.textContent == "")) {
            var random_number = 4;
        } else if (((block_1.textContent) == user) && (block_8.textContent == user) && (block_6.textContent == "")) {
            var random_number = 6;
        } else if (((block_3.textContent) == user) && (block_8.textContent == user) && (block_6.textContent == "")) {
            var random_number = 6;
        } else if (((block_3.textContent) == user) && (block_8.textContent == user) && (block_4.textContent == "")) {
            var random_number = 4;
        } else if (((block_9.textContent) == user) && ((block_5.textContent) == user) && ((block_3.textContent) == "")) {
            var random_number = 3;
        } else if (((block_7.textContent) == user) && ((block_5.textContent) == user) && ((block_1.textContent) == "")) {
            var random_number = 1;
        } else if (((block_1.textContent) == user) && ((block_5.textContent) == user) && ((block_7.textContent) == "")) {
            var random_number = 7;
        } else if (((block_3.textContent) == user) && ((block_5.textContent) == user) && ((block_9.textContent) == "")) {
            var random_number = 9;
        } else if ((two_border_over()) == true) {
            var random_number = random_number_excluding_border();
        } else if (((block_6.textContent) == user) && ((block_8.textContent) == user)) {
            var random_number = 9;
        } else if (((block_4.textContent) == user) && ((block_8.textContent) == user)) {
            var random_number = 7;
        } else if (((block_2.textContent) == user) && ((block_4.textContent) == user)) {
            var random_number = 1;
        } else if (((block_2.textContent) == user) && ((block_6.textContent) == user)) {
            var random_number = 3;
        } else if ((remaining_border = any_border_remaining()) != -1) {
            var random_number = remaining_border;
        } else {
            var random_number = (1 + Math.round(Math.random() * 8));
        }
    } while ((array_of_over_user.includes(random_number)) || (array_of_over_cpu.includes(random_number)));


    array_of_over_cpu.push(random_number);
    document.querySelector(".block-" + random_number).textContent = computer;
    if (array_of_over_cpu.length >= 3) {
        for (var i = 0; i < arrays.length; i++) {
            if (containsOtherArray(array_of_over_cpu, arrays[i])) {
                //win
                gameover.textContent = "You lose the game!!!";
                displayLine();
                gameover.style.display = 'block';
                main_heading.style.display = 'none';
                const lose = localStorage.getItem("lose");
                //It will return 0 ,even if the lose is not declared with the setItem!!!
                const totalloses = Number(lose) + 1;
                localStorage.setItem('lose', totalloses);
                return;
            }
        }
    }
}

//Inside this function if the bigarray contains all the elements of the samllArray then it will return true!!!
function containsOtherArray(bigArray, smallArray) {
    var contain = false;
    for (let j = 0; j < smallArray.length; j++) {
        if (bigArray.includes(smallArray[j])) {
            contain = true;
        } else {
            contain = false;
            return contain;
        }
    }
    return contain;
}


function x_button_click(e) {
    user = "X";
    computer = "0";
    main_box.style.display = 'grid';
    x_button.style.display = 'none';
    o_button.style.display = 'none';
    center.style.display = 'none';
    restart.style.display = 'inline-block';
    reset.style.display = 'inline-block';
    backgrd_res.style.top = '551px';
    result_big.style.top = '555px';
    main_heading.style.display = 'block';
}

function o_button_click() {
    user = "0";
    computer = "X";
    main_box.style.display = 'grid';
    x_button.style.display = 'none';
    o_button.style.display = 'none';
    center.style.display = 'none';
    restart.style.display = 'inline-block';
    reset.style.display = 'inline-block';
    backgrd_res.style.top = '551px';
    result_big.style.top = '555px';
    main_heading.style.display = 'block';
}

function restart_click() {
    window.location.reload();
}

function reset_click() {
    for (var i = 1; i < 10; i++) {
        document.querySelector(".block-" + i).textContent = "";
    }
    array_of_over_user = [];
    array_of_over_cpu = [];
    gameover.style.display = 'none';
    gameover.textContent = "";

    for (var i = 1; i < 4; i++) {
        document.querySelector(".line" + i).style.width = 0;
    }
    for (var i = 7; i < 9; i++) {
        document.querySelector(".line" + i).style.height = 0;
    }
    for (var i = 4; i < 7; i++) {
        document.querySelector(".line" + i).style.height = 0;
    }
    oddeven = 0;
    main_heading.style.display = 'block';
}

function cross_button_click() {
    document.querySelector(".card-panel").style.display = 'none';
}

function results_display() {
    document.querySelector(".card-panel").style.display = 'block';
    total_win.textContent = "Win :  " + localStorage.getItem("win");
    total_lose.textContent = "Lose :  " + localStorage.getItem("lose");
    total_tied.textContent = "Tie :  " + localStorage.getItem("tied");
}
